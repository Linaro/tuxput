.PHONY:all flake8 check-black black
all: check-black flake8
	@echo "🚀😀👌😍🚀"

check-black:
	black --check .

flake8:
	flake8 tuxput tpcli

black:
	black .
