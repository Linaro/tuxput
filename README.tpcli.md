# tpcli

tuxput CLI tool

tpcli is a command line tool meant to assist in uploading files to a tuxput
serverless instance.  tuxput provides a python/flask front end to S3 storage,
but allows you to distribute an API key that you control and specify locations
where that key is allowed to upload.


# Installation

tpcli is currently distributed with tuxput.  In order to install tuxput, we
suggest using the `pipenv` tool to create virtual environments and manage
the python packages installed there.

The easiest way to install is to run:

```shell
pip install tuxput
```

Alternatively, you may download the repository from https://gitlab.com/Linaro/tuxput.git and run "python setup.py install" to install it on your system.


# Uploading Files

To upload files, you would run:

```shell
tpcli -t APIKEY https://files.example.com/upload/PATH FILE_OR_DIR
```


## Options

tpcli supports the following options:

  -n, --dry-run       show steps to be executed, but do not upload anything
  -t, --token TEXT    identification token for the tuxput server
  -b, --bucket TEXT   name of the S3 for the upload
  -V, --version
  -l, --list_buckets  list the buckets available through tuxput
  -A, --list_auths    list the authorized buckets and paths for this token
  -s, --sign-only     generate and print signed urls for each TARGET, but do
                      not upload

### Environment Variables

You may set the following environment variables instead of specifying them on
the command line as an option:

  TPCLI_TOKEN          identification token for the tuxput server
  TPCLI_BUCKET         name of the S3 bucket for the upload


### APIKEY

The token that the administrator of the tuxput server will give you in order to identify yourself to the server.


### PATH

The path to the folder in S3 where the target will be uploaded.

Note that the "/upload" portion of the server URL will always be required.  It is actually the REST method that is being called.  The rest of the URL will be used as the desired path inside of the S3 for your file/dir to be uploaded.

For example:  If you specify the URL and upload target of: "https://tuxput.example.com/upload/foo/bar/baz blah"

The the file "blah" will be uploaded to "s3://tuxput-bucket/foo/bar/baz/blah"


### FILE_OR_DIR

If you specify a file as the target to upload, that file will be uploaded as
the same name on the server.

Example 1: "URL/upload/docs README.md" creates "s3://bucket/docs/README.md"
Example 2: "URL/upload/icons png/error.png" creates "s3://bucket/icons/png/error.png"

If you specify a directory as the target, tpcli will recurse through the
directory and upload each file it finds as a corresponding S3 object.
